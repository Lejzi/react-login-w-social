
import React, { Component } from 'react';


class LoginBox extends Component {
  state = {
    email: "",
    password: ""
  }

  handleChange = e => {

  }

  render() {
    return (
      <div className="login-box">
        <h2 className="login-text">Log in</h2>

        <label htmlFor="email">
          <input type="email" id="email" />
        </label>
        <label htmlFor="password">
          <input type="password" id="password" />
        </label>
      </div>
    );
  }
}

export default LoginBox;