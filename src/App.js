import React, { Component } from 'react';
import './App.css';
import LoginBox from './components/LoginBox'
import Dashboard from './components/Dashboard'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

class App extends Component {
  state = {
    isLoggedIn: false
  }
  render() {
    return (
      <Router>
        <div className="App">
        </div>
        <div className="main">
          <Switch>
            <Route exact path='/' component={Dashboard} />
            <Route path='/login' component={LoginBox} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;